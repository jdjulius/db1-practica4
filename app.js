var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var pg = require('pg');
var conString = "tcp://postgres:jdjulio@localhost:5432/Practica4";
var client = new pg.Client(conString);
client.connect();

var routes = require('./routes/index');
var users = require('./routes/users');
var informacion = require('./routes/Informacion');
var reservacion = require('./routes/Reservacion');
var Bus = require('./routes/Bus');
var Pago = require('./routes/Pago');
var Viaje = require('./routes/Viaje');
var Ruta = require('./routes/Ruta');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);
app.use('/Informacion', informacion);
app.use('/Reservacion', reservacion);
app.use('/Bus', Bus);
app.use('/Pago', Pago);
app.use('/Viaje',Viaje);
app.use('/Ruta',Ruta);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
